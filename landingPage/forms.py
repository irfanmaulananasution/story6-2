from django import forms
from .models import StatusModels

class StatusForms(forms.ModelForm):
	status = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "form-control",
		"placeholder" : "How is your day fella?!",
		"required" : True
	}))
	
	class Meta:
		model = StatusModels
		fields = '__all__'
		